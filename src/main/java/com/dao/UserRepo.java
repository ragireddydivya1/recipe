package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.User;

@Repository
public interface UserRepo extends JpaRepository<User,Integer> {

	 @Query("select b from User b where b.email= :email AND b.password= :password")
	 User validateCustLogin(@Param("email") String username, @Param("password") String password);
}
